
{//websocket
// 	 var socket = new WebSocket('ws://echo.websocket.org');//wss secure
	 var socket = new WebSocket('ws://localhost:8889/ws');
}

{//image parameters
//image update frequency
// 	1000;//1 second		3000;//3 seconds	60000;//1 minute	180000;//3 minutes	600000;//10 minutes	3600000;//60 minutes
	var imageUpdateTime = 180000;//3 minutes

//camera distance
	var camera_distance = 464;			//200;		//500
//plane y position	
	var panorama_y_position = 44;			//210;		//0;
	var panorama_z_position = -camera_distance;	//-450		//-450;
	
//sliding parameter (12.05 is the maximum speed and full circle 360 degrees)
	var rotation_step = 12.05;//12.05;//how fast is it sliding (calibrated to have no jumps when crossing zero)
}

{//live cam source
	//test
// 	var sourcefile = 'Images/test/preset_1k_b.jpg';//preset_1k.jpg								//1k
// 	var sourcefile = 'http://jennersdorf.dyndns.org/record/current.jpg';							//0.3k (fast update visible)
	
	//ALMA
// 	var sourcefile = 'http://www.eso.org/public/archives/static/almapano/latest/pano_PV.jpg';				//1k
// 	var sourcefile = 'http://www.eso.org/public/archives/static/almapano/latest/POI/preset_100.jpg';			//10k 
	var sourcefile = 'file:///I/Image_Cache/lowres_ALMA-4000.jpg';								//variable width via download-and-resize.sh	
	
	//Paranal
// 	var sourcefile = 'http://www.eso.org/public/archives/static/pano/latest/pano_PV.jpg';					//1k
// 	var sourcefile = 'http://www.eso.org/public/archives/static/pano/latest/POI/preset_100.jpg';//or https 		//10k
// 	var sourcefile = 'file:///I/Image_Cache/lowres_Paranal-4000.jpg';							//variable width via download-and-resize.sh  
}

{//slider (to test panning the image with echo server)
	var sliderOn = false;   
}

{//websocket result output div visibility
	var websocketdiv = document.getElementById('result');
// 	websocketdiv.style.visibility = 'visible';
	websocketdiv.style.visibility = 'hidden';
}