#!/bin/bash

xrandr --output HDMI-1 --mode 1920x1080 --pos 0x0 --rotate right
xinput map-to-output 'Sharp Corp.   TPC-IC   USB HID' HDMI-1
xinput set-prop 'Sharp Corp.   TPC-IC   USB HID' 'Coordinate Transformation Matrix' 0 1 0 -1 0 1 0 0 1

exec "$@"
exit $?

