# 0702 - Paranal / ALMA Webcam

## Short Description

<table align="center">
    <tr>
    <td align="left" style="font-style:italic; font-size:12px; background-color:white">Watch the Paranal / ALMA observatory live!<br>
        Turn the screen to look around!</td>
    </tr>
</table>

The application shows 360 degrees web camera view from one of the two locations in the Chilean Atacama desert: ESO Paranal Observatory or the Atacama Large Millimeter Array (ALMA). Turning the screens around their axis turns the panoramic image. The user can also see the current time in Chile. There is a time lapse video available for the Paranal Observatory location.

In the exhibition two portrait-oriented screens are mounted on a pole. By turning the screens around their axis the visible section of the panoramic images smoothly changes following the turning of the screen. By selecting the movie button (Paranal Observatory only) the user can see a panoramic time lapse video. The upper left corner of the screen shows the current time in Chile and in Germany. Three buttons in the lower left corner allow the user to restart the application, change the language (English or German) or get some help. 

This application is used at the [ESO Supernova Planetarium and Visitor Centre](https://supernova.eso.org/?lang=en), Garching b. München.  
For more details about the project and how applications are run and managed within the exhibition please see [this link](https://gitlab.com/HITS_Supernova/overview).   

## Requirements / How-To

**WebGL**  
A browser with a WebGL support is needed to run the interactive (start `WebGL/webgl_WebCam.html` or `WebGL_ALMA/webgl_WebCam.html`).  
In order to see the images from the webcam without using the Python part of the interactive change the 'sourcefile' within parameters.js to one of the eso sites e.g. 'https://www.eso.org/public/archives/static/pano/latest/pano_PV.jpg'. 

**Mouse motion server**  
The [Mouse Motion Server](https://gitlab.com/HITS_Supernova/0000_mousemotionserver) background application detects any rotation of the screen around the pole axis by mouse motion events of the X server. It serves an orientation angle (0 ... 360 degrees) though a websocket (default: TCP port 8889). The scaling parameter (translating motion in pixels to changes in angle) should be set with the environment variable `HILBERT_MOTION_SCALING`. More details are given in the `README.md` of that repository.

## Detailed Information

#### Time Zones

Due to different dates of daylight saving time in Germany and Chile, it is currently difficult to calculate the exact time difference between the two countries.  
It varies between 4, 5 and 6 hours of difference during the entire year.  
Current times at Germany and Chile are calculated correctly until 2022.  
See [Moment Timezone](https://momentjs.com/timezone/).

#### URL parameters (webpage)

*lang* - language parameter (english as default if not there)  

#### Client/server communication 

WebSockets transfer the rotation parameter (from python part to the webpage).  

    Websockets port: 8889  

#### (Optional) Using the Additional Scripts

Applications within the [Hilbert](https://github.com/hilbert) Museum Management System are used as [Docker](https://www.docker.com/) containers.   
Dockerized version of the application uses the `Dockerfile`, `Makefile` and various bash scripts to properly start the station.  

Application uses the following scripts to start: 

* `docker-compose.hitsparanalwebcam.yml` (yml file not available):
    * starts `start_background_scripts.sh`:  
        * starts `download-and-resize.sh` (dealing with webcam images within Image_Cache folder)
        * starts python mouse controller (Python folder)  
    * starts `rearrange.sh` - rotates the screen view to portrait
    * Hilbert kiosk browser is running the webgl webcam viewer code
    
* `Dockerfile` additionally does the following:
    * add the timelapse video  


## Credits

This application was developed by the ESO Supernova team at [HITS gGmbH](https://www.h-its.org/en/).  
Idea by Dorotea Dudas and Volker Gaibler, HITS gGmbH.  
WebGL webcam application by Dorotea Dudas.  
Python mouse reading / websockets application by Volker Gaibler.  

#### Code Licensing

* This code is licensed as: [MIT license](LICENSE)
* MIT license:
    * *jQuery* [source](https://jquery.com/)
    * *Three.js* by Mr.doob (Ricardo Cabello) [source](https://threejs.org/)
    * *THREEx.KeyboardState.js* by Jerome Etienne [threex.keyboardstate](https://github.com/jeromeetienne/threex.keyboardstate)
    * *moment.js*, *moment-timezone.js* [Moment.js](https://momentjs.com/) [Moment Timezone](https://momentjs.com/timezone/) 
    * *Image Blend shader* (intrinsic) by Dorotea Dudas


#### Image / Video Licensing 

* CC BY 4.0:
    * Live Paranal / ALMA webcams by ESO
    * We currently do not have a free video available, and are therefore providing a "dummy" version only (`WebGL/Images/video_paranal.mp4`)  
(note: video is available at the exhibition, low resolution version can be seen on [youtube](https://www.youtube.com/watch?v=g77CxWquJEU)))
    * Info/Help Screen images by ESO / HITS gGmbH
    * Icons (except blue navigation icons) by Dorotea Dudas
    * Blue Navigation icons by Design und mehr GmbH 









