#!/bin/bash

echo "Starting download-and-resize script..."
/I/download-and-resize.sh &

echo "Starting python server for websocket..."
/I/Python/start.sh &

# give background scripts a few seconds to print their initial texts, then continue
sleep 3

exec "$@"
exit $?

