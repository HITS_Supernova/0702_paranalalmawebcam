#!/bin/bash
#
# downloads high-resolution webcam image from URL to site-specific file and
# reduces the resolution at regular intervals
#
# VG 2018

# environment variable defaults
HILBERT_WEBCAM_SITE=${HILBERT_WEBCAM_SITE:-ALMA}
HILBERT_WEBCAM_URL=${HILBERT_WEBCAM_URL:-'https://www.eso.org/public/archives/static/almapano/latest/POI/preset_100.jpg'}
HILBERT_WEBCAM_TIMESTAMP=${HILBERT_WEBCAM_TIMESTAMP:-'https://www.eso.org/public/archives/static/almapano/latest/timestamp.txt'}

# high-res filename
highres_file="highres_$HILBERT_WEBCAM_SITE.jpg"

# lowres-sizes (widths, space-separated)
lowres_sizes="4000 2000"

# low-res filename prefix (size and .jpg will be appended)
lowres_file="lowres_$HILBERT_WEBCAM_SITE"

# timestamp file
timestamp_file="timestamp_$HILBERT_WEBCAM_SITE.txt"

# sleep delay in seconds
delay=60

# annotation font
font="Helvetica-Neue-LT-Com-45-Light"
fontsize=20


#############################################################

LC_ALL=C
wd="$( dirname $(readlink -e $0) )"
mkdir -p "$wd/Image_Cache"
cd "$wd/Image_Cache"

echo "HILBERT_WEBCAM_SITE=$HILBERT_WEBCAM_SITE"
echo "HILBERT_WEBCAM_URL=$HILBERT_WEBCAM_URL"
echo

while true ; do

	date=$(date -Is)	
	echo "[+] Checking URL ...  $date"

	highres_tmpfile=$( basename "$HILBERT_WEBCAM_URL" )
        wget_log=wget.log
	# To use timestamping (Last-Modified), we cannot use "-O" and write to tmpfile
	# Without "-O", wget should download to $highres_tmpfile

	# tmp fix because timestamping not working with ESO server... :-(
        rm -f "$highres_tmpfile"  # remove before wget
	wget \
		--progress=dot:mega \
                --output-file="$wget_log" \
		--timestamping \
		"$HILBERT_WEBCAM_URL"
		#--limit-rate=100k \
                #--output-document="$highres_tmpfile" \
		#
	ret=$?
	echo "wget log output:"
	cat "$wget_log"

	# did wget actually download or tell that there is nothing new?
	if grep -q " saved " $wget_log ; then
		saved=true
	else
		saved=false
	fi
	#echo "wget: $ret, saved: $saved"

	if [[ $ret = 0 && $saved = true ]] ; then

		echo "[+] wget successful and downloaded new image. Copying into high resolution file '$highres_file' ..."

		# get timestamp file: when was image taken?
		wget -q "$HILBERT_WEBCAM_TIMESTAMP" -O "$timestamp_file" \
			&& time=$(<$timestamp_file) \
			&& echo "[+] Downloaded timestamp file '$timestamp_file'." || echo "ERROR: Could not download timestamp"
		echo "[+] Timestamp from file: '$time'."

		# replace only if successful
		cp -f "$highres_tmpfile" "$highres_file"

		echo "[+] Creating low resolution versions ..."
		tmpfile="tmpfile"
		for sz in $lowres_sizes ; do
                        file="$lowres_file-$sz.jpg"
			echo -n "    - $file (width $sz, keep aspect) ... "
			convert "$highres_file" \
				-resize "$szx$sz>" \
				-font "$font" \
				-pointsize $fontsize \
				-gravity south \
				-stroke '#00000050' -strokewidth 3 -annotate +0+10 "$time" \
				-stroke none -fill white -annotate +0+10 "$time" \
				-quality 100 \
				"$tmpfile" \
				&& mv "$tmpfile" "$file" && echo "      OK" || echo "      ERROR"
		done

	fi
	
	echo
	sleep $delay
done



